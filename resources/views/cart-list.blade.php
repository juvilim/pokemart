@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading" align="center">Your Cart</div>

                    <div class="panel-body">
                        <div class="products">
                            <div class="products-content-wrapper">

                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th>Sub Total</th>
                                        <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($items as $item)
                                        <tr>
                                            <td><img src="{{ asset($item->image) }}"></td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $cart[$item['id']] }}</td>
                                            <td>{{ $item->price }}</td>
                                            <td>{{ $cart[$item['id']] * $item->price }}</td>
                                            <td>
                                                <a href="{{ url('/cart/delete/'. $item->id) }}">
                                                    <button class="btn btn-primary" type="submit">Delete</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                <br>
                                <div>
                                    <label>Total Quantity:</label>
                                    {{ $totalQty }}
                                </div>
                                <br>
                                <div>
                                    <label>Total Price:</label>
                                    {{ $totalPrice }}
                                </div>
                                <br><br>
                                <a href="{{ url('/cart/payment/pending') }}">
                                    <input type="submit" class="btn btn-primary" value="Complete the Payment">
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
