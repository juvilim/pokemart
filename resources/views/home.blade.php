@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if (Auth::check())
                    Hi, {{ Auth::user()->email }}
                </div>
                <div class="panel-body">
                    You are logged in!
                </div>
                    @else
                        Hi, Guest
                </div>
                <div class="panel-body">
                    You are not logged in!
                </div>
                    @endif


            </div>
        </div>
    </div>
</div>
@endsection
