@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome to PokeMart, {{ date('D-d M Y') }}</div>

                <div class="panel-body">

                    PokeMart is an online store made in Indonesia. Established in 2017. <br>
                    @if (Auth::check())
                        <a class="btn-update" href="{{ url('/profile/edit') }}">User Profile</a>
                        <br>
                        <a class="btn-update" href="{{ url('/pokemon/list') }}">Pokemon List</a>
                        <br>
                        <a class="btn-update" href="{{ url('/cart/list') }}">Your Cart</a>
                        <br><br>
                        {{--@if (auth()->user()->isAdmin())--}}
                            <a class="btn-update" href="{{ url('/element') }}">Element (Admin)</a>
                            <br>
                            <a class="btn-update" href="{{ url('/pokemon/list/admin') }}">Pokemon List (Admin)</a>
                            <br>
                            <a class="btn-update" href="{{ url('/pokemon/insert') }}">Pokemon Insert (Admin)</a>
                            {{--<br>--}}
                            {{--<a class="btn-update" href="{{ url('/pokemon/update') }}">Pokemon Update (Admin)</a>--}}
                            <br>
                            <a class="btn-update" href="{{ url('/pokemon/delete') }}">Pokemon Delete (Admin)</a>
                            <br>
                            <a class="btn-update" href="{{ url('/user/update') }}">User Update (Admin)</a>
                            <br>
                            <a class="btn-update" href="{{ url('/user/delete') }}">User Delete (Admin)</a>
                            <br>
                            <a class="btn-update" href="{{ url('/transaction/list/update') }}">Transaction Update (Admin)</a>
                            <br>
                            <a class="btn-update" href="{{ url('/transaction/list/delete') }}">Transaction Delete (Admin)</a>

                        {{--@endif--}}
                    @endif

<br>


</div>

</div>
</div>
</div>
</div>
@endsection
