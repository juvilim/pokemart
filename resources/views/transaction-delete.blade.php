@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading" align="center">Delete Transaction</div>

                    <div class="panel-body">
                        <div class="products">
                            <div class="products-content-wrapper">

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Email</th>
                                        <th>Datetime</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($transactions as $transaction)
                                        <tr>
                                            <td>{{ $transaction->id }}</td>
                                            <td>{{ $transaction->email }}</td>
                                            <td>{{ $transaction->date }}</td>
                                            <td>{{ $transaction->status }}</td>
                                            <td>
                                                <form action="{{ url('/transaction/list/delete/'. $transaction->id) }}" method="POST">
                                                {{ CSRF_FIELD() }}
                                                {{ method_field('DELETE') }}
                                                <button class="btn btn-primary">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>


                                {{--<img class="img-products" src="{{ asset($pokemon[0]->image) }}" alt="pokemon image"> <!-- for image, use source from image column in database -->--}}
                                {{--<div class="description-wrapper">--}}
                                {{--<p>Price : {{ $pokemon[0]->price }}</p>--}}
                                {{--<p>Type : {{ $pokemon[0]->element_type }}</p>--}}
                                {{--<p>Gender : {{ $pokemon[0]->gender }}</p>--}}
                                {{--<br>--}}
                                {{--<p>{{ $pokemon[0]->description }}</p>--}}
                                {{--<br>--}}
                                {{--<p>Coments: </p>--}}
                                {{--<hr>--}}
                                {{--@foreach ($comments as $comment)--}}
                                {{--<p><span style="float: left;">{{ $comment->email }}</span><span style="float: right">{{ $comment->date }}</span></p>--}}
                                {{--<br>--}}
                                {{--<p>{{ $comment->comments }}</p>--}}
                                {{--@endforeach--}}
                                {{--</div>--}}
                                {{--<br><br>--}}
                                {{--<hr>--}}
                                {{--<div style="text-align: center;">--}}
                                {{--Qty <input type="number" id="qty" name="qty" style="width: 50px;" value="{{ session('qty') }}" required />--}}
                                {{--<a class="addToCart" href="{{ url('/cart/' . $pokemon[0]->id) }}">--}}
                                {{--<input type="submit" value="Add To Cart">--}}
                                {{--</a>--}}
                                {{--</div>--}}
                                {{--<br>--}}
                                {{--<form method="POST" action="{{ url('/pokemon/detail/' . $pokemon[0]->id . '/comment') }}" style="margin: 5px">--}}
                                {{--{!! csrf_field() !!}--}}
                                {{--<input type="text" name="comment" class="form-control" placeholder="Insert your comment here...">--}}
                                {{--<br>--}}
                                {{--<input type="submit" class="btn btn-primary" value="Insert Comment">--}}
                                {{--</form>--}}

                                {{--<form action="{{ url('/delete/' . $car['car_id']) }}" method="POST">--}}
                                {{--{{ CSRF_FIELD() }}--}}
                                {{--{{ method_field('DELETE') }}--}}
                                {{--<a class="btn-delete" href="{{ url('/delete/' . $pokemon['car_id']) }}">Delete</a> <!-- Direct to delete with car id -->--}}

                                {{--</form>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
