@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading" align="center">Update Transaction</div>

                    <div class="panel-body">
                        <div class="products">
                            <div class="products-content-wrapper">

                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Email</th>
                                            <th>Date</th>
                                            <th>Status</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($transactions as $transaction)
                                        <tr>
                                            <td>{{ $transaction->id }}</td>
                                            <td>{{ $transaction->email }}</td>
                                            <td>{{ $transaction->date }}</td>
                                            <td>{{ $transaction->status }}</td>
                                            <td>
                                                <form action="{{ url('/transaction/list/update/'. $transaction->id) }}" method="POST">
                                                    {{ CSRF_FIELD() }}
                                                    {{ method_field('PUT') }}
                                                    <input name="status" type="submit" class="btn btn-primary" value="Accept">
                                                    {{--<button name="status" class="btn btn-primary" value="accept">Accept</button>--}}
                                                </form>
                                            </td>
                                            <td>
                                                <form action="{{ url('/transaction/list/update/'. $transaction->id) }}" method="POST">
                                                    {{ CSRF_FIELD() }}
                                                    {{ method_field('PUT') }}
                                                    <input name="status" type="submit" class="btn btn-primary" value="Decline">
                                                    {{--<button class="btn-update" >Decline</button>--}}
                                                </form>
                                            </td>
                                            <td>
                                                <a href="{{ url('/transaction/detail/' . $transaction->id) }}">
                                                    <button class="btn btn-primary" type="submit">Display</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
