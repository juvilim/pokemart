@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" align="center">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Pokemon List</div>

                    <div class="content col-lg-12 col-xl-12">

                        <form method="GET" action="{{ url('/pokemon/list/')  }}" class="form-inline" style="margin: 5px">
                            <input type="text" name="keyword" value="{{ $keyword }}" class="form-control" placeholder="By Name, By Element">
                            <input type="submit" class="btn btn-primary" value="Search">
                            <select name="searchCategory" id="searchCategory" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{ $category }}" @if (session('category') == $category) selected="selected" @endif>{{ $category }}</option>
                                @endforeach
                            </select>
                        </form>

                    </div>
                    @for($i=0; $i < count($pokemons); $i++)
                        <div class="content col-lg-1 col-xl-1">
                            <div class=content-wrapper" style="margin-left: -8px;padding: 8px 0;">
                                <img src="{{ asset($pokemons[$i]->image) }}"  style="width: 80px; height: 80px" alt="pokemon icon">
                                <div>
                                    <p>{{ $pokemons[$i]->name }}</p>
                                    <form action="{{ url('/pokemon/delete/' . $pokemons[$i]->id) }}" method="POST">
                                    {{ CSRF_FIELD() }}
                                    {{--{{ method_field('DELETE') }}--}}
                                    <a class="btn-delete" href="{{ url('/pokemon/delete/' . $pokemons[$i]->id) }}" style="background-color: whitesmoke;width: 49%;
                                       border: none;
                                       color: #737373;
                                       padding: 7px 10px;
                                       text-align: center;
                                       text-decoration: none;
                                       font-size: 16px;
                                       cursor: pointer;
                                       border-radius: 5px;
                                       margin-right: 5px;">Delete</a>

                                    </form>
                                </div>
                            </div>
                        </div>
                    @endfor

                </div>

            </div>

        </div>
        <div class="row" align="center">
            {{ $pokemons->appends(Request::except('page'))->links() }}
        </div>
    </div>
@endsection
