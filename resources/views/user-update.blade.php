<html>
<head>
    <title>User Update</title>
</head>
<body>
    <h2>Update User</h2>
    <br>
    <form action="{{ url('/user/update/' . $user->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <img class="img-products" src="{{ asset($user->profilePicture) }}" alt="profile picture" style="width: 200px; height: 200px">
        <br>
        <input type="file" name="profilePicture" placeholder="Browse..">
        <br>
        <label>Email: </label>
        <input type="text" name="email" value="{{ $user->email }}">
        <br>
        <label>Gender: </label>
        <div>
            @if ($user->gender == "male")
                <input id="gender" type="radio" name="gender" value="male" checked="checked">Male
                <input id="gender" type="radio" name="gender" value="female">Female
            @elseif ($user->gender == "female")
                <input id="gender" type="radio" name="gender" value="male">Male
                <input id="gender" type="radio" name="gender" value="female" checked="checked">Female
            @endif
        </div>
        <br>
        <label>Date of Birth: </label>
        <input id="dateOfBirth" type="text" class="form-control" name="dateOfBirth" value="{{ $user->dateOfBirth }}">
        <br>
        <label>Address: </label>
        <textarea name="address" cols="46" rows="5">{{ $user->address }}</textarea>
        <br><br>
        <input type="submit" class="btn btn-primary" value="Edit">
    </form>
    @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            {{ $error }} <br>
        @endforeach
    @endif
</body>
</html>