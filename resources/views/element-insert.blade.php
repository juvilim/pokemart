<html>
<head>
    <title>Element Insert</title>
</head>
<body>
    <h2>Insert Element</h2>

    <form action="{{ url('/element') }}" class="form-inline" method="post">
        {!! csrf_field() !!}
        <input type="text" name="name" placeholder="Element Name">
        <input type="submit" class="btn btn-primary" value="Insert Element">
    </form>
    @if ($errors->has('name'))
        <div class="error">{{ $errors->first('name') }}</div>
    @endif
</body>
</html>