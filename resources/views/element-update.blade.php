<html>
<head>
    <title>Element Update</title>
</head>
<body>
    <h2>Update Element</h2>
    <br>
    <form action="{{ url('/element/' . $element->id) }}" method="post">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <input type="text" name="name" value="{{ $element->name }}">
        <input type="submit" class="btn btn-primary" value="Edit">
    </form>
    @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            {{ $error }} <br>

        @endforeach
    @endif
</body>
</html>