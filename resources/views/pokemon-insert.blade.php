<html>
<head>
    <title>Pokemon Insert</title>
</head>
<body>
    <h2>Insert Pokemon</h2>
    <br>
    <form action="{{ url('/pokemon/insert') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="text" name="name" placeholder="Your Pokemon Name">
        <br>
        <select name="element" class="form-control">
            <option value="">-- Choose Element --</option>
            @foreach($elements as $element)
                <option value="{{ $element->id }}" @if (session('element') == $element->name) selected="selected" @endif>{{ $element->name }}</option>
            @endforeach
        </select>
        <br>
        <input type="file" name="image" placeholder="Browse..">
        <br>
        <div>
            <input id="gender" type="radio" name="gender" value="Male">Male
            <input id="gender" type="radio" name="gender" value="Female">Female
        </div>
        <br>
        <textarea name="description" cols="46" rows="5" placeholder="Description"></textarea>
        <br>
        <input type="text" name="price" placeholder="Price">
        <br><br>
        <input type="submit" class="btn btn-primary" value="Insert New Pokemon">
    </form>
    @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            {{ $error }} <br>

        @endforeach
    @endif
</body>
</html>