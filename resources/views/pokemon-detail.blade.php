@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading" align="center">{{ $pokemon[0]->name }}</div>

                    <div class="panel-body">
                            <div class="products">
                                <div class="products-content-wrapper">
                                    <img class="img-products" src="{{ asset($pokemon[0]->image) }}" alt="pokemon image"> <!-- for image, use source from image column in database -->
                                    <div class="description-wrapper">
                                        <p>Price : {{ $pokemon[0]->price }}</p>
                                        <p>Type : {{ $pokemon[0]->element_type }}</p>
                                        <p>Gender : {{ $pokemon[0]->gender }}</p>
                                        <br>
                                        <p>{{ $pokemon[0]->description }}</p>
                                        <br>
                                        <p>Coments: </p>
                                        <hr>
                                        @foreach ($comments as $comment)
                                            <p><span style="float: left;">{{ $comment->email }}</span><span style="float: right">{{ $comment->date }}</span></p>
                                                <br>
                                            <p>{{ $comment->comments }}</p>
                                        @endforeach
                                    </div>
                                    <br><br>
                                    <hr>
                                    <form method="GET" action="{{ url('/cart/add/' . $pokemon[0]->id) }}" style="margin: 5px; text-align: center">
                                        {!! csrf_field() !!}
                                        Qty <input type="number" name="qty" style="width: 50px;" value="0" required />
                                        <input type="submit" value="Add To Cart">
                                        <br>
                                        @if(count($errors) > 0)
                                            @foreach($errors->all() as $error)
                                                {{ $error }} <br>

                                            @endforeach
                                        @endif
                                    </form>

                                    <br><br>
                                    <form method="POST" action="{{ url('/pokemon/detail/comment/' . $pokemon[0]->id) }}" style="margin: 5px">
                                        {!! csrf_field() !!}
                                        <input type="text" name="comment" class="form-control" placeholder="Insert your comment here...">
                                        <br>
                                        @if(count($errors) > 0)
                                            @foreach($errors->all() as $error)
                                                {{ $error }} <br>

                                            @endforeach
                                        @endif
                                        <br>
                                        <input type="submit" class="btn btn-primary" value="Insert Comment">
                                    </form>

                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
