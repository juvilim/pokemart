<html>
<head>
    <title>Element</title>
</head>
<body>
    <h2>Element List</h2>
    <br>
    <form method="GET" action="{{ url('/element/edit/') }}" class="form-inline" style="margin: 5px">
        <select name="searchElement" class="form-control">
            <option value="">-- Choose Element --</option>
            @foreach($elements as $element)
                <option value="{{ $element->id }}" @if (session('element') == $element->name) selected="selected" @endif>{{ $element->name }}</option>
            @endforeach
        </select>
        <input type="submit" class="btn btn-primary" value="Search">
    </form>
    @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            {{ $error }} <br>

        @endforeach
    @endif
    <br>
    <a class="btn-insert" href="{{ url('/element/create') }}">
        <input type="submit" value="Insert Element">
    </a>

</body>
</html>