<html>
<head>
    <title>Pokemon Update</title>
</head>
<body>
<h2>Update Pokemon</h2>
<form action="{{ url('/pokemon/update/' . $pokemon[0]->id) }}" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <img class="img-products" src="{{ asset($pokemon[0]->image) }}" alt="pokemon image">
    <br>
    <input type="file" name="image" placeholder="Browse..">
    <br>
    <label>Name: </label>
    <input type="text" name="name" value="{{ $pokemon[0]->name }}">
    <br>
    <label>Element: </label>
    <select name="element" class="form-control">
        @foreach($elements as $element)
            <option value="{{ $element->id }}" name="tes" @if (session('element') == $element->name) selected="selected" @endif>{{ $element->name }}</option>
        @endforeach
    </select>
    <br>
    <label>Gender: </label>
    <div>
        @if ($pokemon[0]->gender == "Male")
            <input id="gender" type="radio" name="gender" value="Male" checked="checked">Male
            <input id="gender" type="radio" name="gender" value="Female">Female
        @elseif ($pokemon[0]->gender == "Female")
            <input id="gender" type="radio" name="gender" value="Male">Male
            <input id="gender" type="radio" name="gender" value="Female" checked="checked">Female
        @endif
    </div>
    <br>
    <label>Description: </label>
    <textarea name="description" cols="46" rows="5">{{ $pokemon[0]->description }}</textarea>
    <br>
    <label>Price: </label>
    <input type="text" name="price" value="{{ $pokemon[0]->price }}">
    <br><br>
    <input type="submit" class="btn btn-primary" value="Edit">
</form>
@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        {{ $error }} <br>

    @endforeach
@endif
</body>
</html>