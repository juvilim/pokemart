<html>
<head>
    <title>User Delete</title>
</head>
<body>
    <h2>Delete User</h2>
    <br>
    <form method="POST" action="{{ url('/user/delete') }}" class="form-inline" style="margin: 5px">
        {{ CSRF_FIELD() }}
        {{ method_field('DELETE') }}
        <select name="searchEmail" class="form-control">
            <option value="">-- Choose Email --</option>
            @foreach($users as $user)
                <option value="{{ $user->id }}" @if (session('email') == $user->email) selected="selected" @endif>{{ $user->email }}</option>
            @endforeach
        </select>
        <input type="submit" class="btn btn-primary" value="Delete User">
    </form>
@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        {{ $error }} <br>

    @endforeach
@endif
</body>
</html>