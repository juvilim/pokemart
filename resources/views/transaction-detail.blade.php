@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading" align="center">Detail Transaction</div>

                    <div class="panel-body">
                        <div class="products">
                            <div class="products-content-wrapper">

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Pokemon Name</th>
                                        <th>Price</th>
                                        <th>Qty</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($transactionDetails as $transactionDetail)
                                        <tr>
                                            <td>{{ $transactionDetail->pokemon_name }}</td>
                                            <td>{{ $transactionDetail->pokemon_price }}</td>
                                            <td>{{ $transactionDetail->quantity }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                                <br>
                                <div>
                                    <label>Total Quantity:</label>
                                    {{ $totalQty }}
                                </div>
                                <br>
                                <div>
                                    <label>Total Price:</label>
                                    {{ $totalPrice }}
                                </div>
                                <br><br>
                                <a href="{{ url('/transaction/list/update') }}">
                                    <input type="submit" class="btn btn-primary" value="Back">
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
