<html>
<head>
    <title>User Update</title>
</head>
<body>
    <h2>Update User</h2>
    <br>
    <form method="GET" action="{{ url('/user/update/search') }}" class="form-inline" style="margin: 5px">
        <select name="searchEmail" class="form-control">
            <option value="">-- Choose Email --</option>
            @foreach($users as $user)
                <option value="{{ $user->id }}" @if (session('email') == $user->email) selected="selected" @endif>{{ $user->email }}</option>
            @endforeach
        </select>
        <input type="submit" class="btn btn-primary" value="Search">
    </form>
    @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            {{ $error }} <br>

        @endforeach
    @endif
</body>
</html>