<?php

use Illuminate\Database\Seeder;

class ElementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('elements')->insert([
            ['name' => 'Normal'],
            ['name' => 'Fire'],
            ['name' => 'Water'],
            ['name' => 'Electric'],
            ['name' => 'Grass'],
            ['name' => 'Ice'],
            ['name' => 'Fighting'],
            ['name' => 'Poison'],
            ['name' => 'Ground'],
            ['name' => 'Flying'],
            ['name' => 'Physic'],
            ['name' => 'Bug'],
            ['name' => 'Rock'],
            ['name' => 'Ghost'],
            ['name' => 'Dragon'],
            ['name' => 'Dark'],
            ['name' => 'Steel'],
            ['name' => 'Fairy']
        ]);
    }
}
