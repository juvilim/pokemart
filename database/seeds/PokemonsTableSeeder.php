<?php

use Illuminate\Database\Seeder;

class PokemonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pokemons')->insert([
            [
                'name' => 'Pikachu',
                'image' => 'pokemon/Pikachu.png',
                'price' => 100000,
                'element_id' => '4',
                'gender' => 'Male',
                'description' => '[Generation 1] Pikachu is an Electric type Pokémon. It is known as the Mouse Pokémon.'
            ],
            [
                'name' => 'Snorlax',
                'image' => 'pokemon/Snorlax.png',
                'price' => 200000,
                'element_id' => '1',
                'gender' => 'Male',
                'description' => '[Generation 1] Snorlax is a Normal type Pokémon. It is known as the Sleeping Pokémon.'
            ],
            [
                'name' => 'Absol',
                'image' => 'pokemon/Absol.png',
                'price' => 100000,
                'element_id' => '16',
                'gender' => 'Male',
                'description' => '[Generation 3] Absol is a Dark type Pokémon. It is known as the Disaster Pokémon. 
                                                Absol has a Mega Evolution, available from X & Y onwards.'
            ],
            [
                'name' => 'Gogoat',
                'image' => 'pokemon/Gogoat.png',
                'price' => 200000,
                'element_id' => '5',
                'gender' => 'Male',
                'description' => '[Generation 6] Gogoat is a Grass type Pokémon. It is known as the Mount Pokémon. 
                                                Gogoat is so large that people are able to ride on its back. 
                                                It\'s very calm, and can form a strong bond with its Trainer\'s 
                                                feelings when its Trainer grabs onto its horns.'
            ],
            [
                'name' => 'Herdier',
                'image' => 'pokemon/Herdier.png',
                'price' => 100000,
                'element_id' => '1',
                'gender' => 'Male',
                'description' => '[Generation 5] Herdier is a Normal type Pokémon. It is known as the Loyal Dog Pokémon.'
            ],
            [
                'name' => 'Kirlia',
                'image' => 'pokemon/Kirlia.png',
                'price' => 180000,
                'element_id' => '18',
                'gender' => 'Male',
                'description' => '[Generation 3] Kirlia is a Psychic/Fairy type Pokémon. It is known as the Emotion Pokémon. 
                                                    Prior to Generation 6, Kirlia was a pure Psychic type.'
            ],
            [
                'name' => 'Lucario',
                'image' => 'pokemon/Lucario.png',
                'price' => 350000,
                'element_id' => '7',
                'gender' => 'Male',
                'description' => '[Generation 4] Lucario is a Fighting/Steel type Pokémon. It is known as the Aura Pokémon. 
                                                    Lucario has a Mega Evolution, available from X & Y onwards.'
            ],
            [
                'name' => 'Marill',
                'image' => 'pokemon/Marill.png',
                'price' => 50000,
                'element_id' => '3',
                'gender' => 'Male',
                'description' => '[Generation 2] Marill is a Water/Fairy type Pokémon. It is known as the Aqua Mouse Pokémon.
                                                    Prior to Generation 6, Marill was a pure Water type.'
            ],
            [
                'name' => 'Squirtle',
                'image' => 'pokemon/Squirtle.png',
                'price' => 100000,
                'element_id' => '3',
                'gender' => 'Male',
                'description' => '[Generation 1] Pikachu is an Electric type Pokémon. It is known as the Mouse Pokémon.'
            ],
            [
                'name' => 'Arceus',
                'image' => 'pokemon/Arceus.png',
                'price' => 200000,
                'element_id' => '1',
                'gender' => 'Male',
                'description' => '[Generation 1] Snorlax is a Normal type Pokémon. It is known as the Sleeping Pokémon.'
            ],
            [
                'name' => 'Zoroark',
                'image' => 'pokemon/Zoroark.png',
                'price' => 100000,
                'element_id' => '16',
                'gender' => 'Male',
                'description' => '[Generation 3] Absol is a Dark type Pokémon. It is known as the Disaster Pokémon. 
                                                Absol has a Mega Evolution, available from X & Y onwards.'
            ],
            [
                'name' => 'Bulbasaur',
                'image' => 'pokemon/Bulbasaur.png',
                'price' => 200000,
                'element_id' => '5',
                'gender' => 'Male',
                'description' => '[Generation 6] Gogoat is a Grass type Pokémon. It is known as the Mount Pokémon. 
                                                Gogoat is so large that people are able to ride on its back. 
                                                It\'s very calm, and can form a strong bond with its Trainer\'s 
                                                feelings when its Trainer grabs onto its horns.'
            ],
            [
                'name' => 'Lickilicky',
                'image' => 'pokemon/Lickilicky.png',
                'price' => 100000,
                'element_id' => '1',
                'gender' => 'Male',
                'description' => '[Generation 5] Herdier is a Normal type Pokémon. It is known as the Loyal Dog Pokémon.'
            ],
            [
                'name' => 'Diancie',
                'image' => 'pokemon/Diancie.png',
                'price' => 180000,
                'element_id' => '18',
                'gender' => 'Male',
                'description' => '[Generation 3] Kirlia is a Psychic/Fairy type Pokémon. It is known as the Emotion Pokémon. 
                                                    Prior to Generation 6, Kirlia was a pure Psychic type.'
            ],
            [
                'name' => 'Mienshao',
                'image' => 'pokemon/Mienshao.png',
                'price' => 350000,
                'element_id' => '7',
                'gender' => 'Male',
                'description' => '[Generation 4] Lucario is a Fighting/Steel type Pokémon. It is known as the Aura Pokémon. 
                                                    Lucario has a Mega Evolution, available from X & Y onwards.'
            ],
            [
                'name' => 'Mudkip',
                'image' => 'pokemon/Mudkip.png',
                'price' => 50000,
                'element_id' => '3',
                'gender' => 'Male',
                'description' => '[Generation 2] Marill is a Water/Fairy type Pokémon. It is known as the Aqua Mouse Pokémon.
                                                    Prior to Generation 6, Marill was a pure Water type.'
            ],
            [
                'name' => 'Meowth',
                'image' => 'pokemon/Meowth.png',
                'price' => 100000,
                'element_id' => '1',
                'gender' => 'Male',
                'description' => '[Generation 5] Herdier is a Normal type Pokémon. It is known as the Loyal Dog Pokémon.'
            ],
            [
                'name' => 'Gardevoir',
                'image' => 'pokemon/Gardevoir.png',
                'price' => 180000,
                'element_id' => '18',
                'gender' => 'Male',
                'description' => '[Generation 3] Kirlia is a Psychic/Fairy type Pokémon. It is known as the Emotion Pokémon. 
                                                    Prior to Generation 6, Kirlia was a pure Psychic type.'
            ],
            [
                'name' => 'Hitmontop',
                'image' => 'pokemon/Hitmontop.png',
                'price' => 350000,
                'element_id' => '7',
                'gender' => 'Male',
                'description' => '[Generation 4] Lucario is a Fighting/Steel type Pokémon. It is known as the Aura Pokémon. 
                                                    Lucario has a Mega Evolution, available from X & Y onwards.'
            ],
            [
                'name' => 'Milotic',
                'image' => 'pokemon/Milotic.png',
                'price' => 50000,
                'element_id' => '3',
                'gender' => 'Male',
                'description' => '[Generation 2] Marill is a Water/Fairy type Pokémon. It is known as the Aqua Mouse Pokémon.
                                                    Prior to Generation 6, Marill was a pure Water type.'
            ],
            [
                'name' => 'Piplup',
                'image' => 'pokemon/Piplup.png',
                'price' => 100000,
                'element_id' => '3',
                'gender' => 'Male',
                'description' => '[Generation 1] Pikachu is an Electric type Pokémon. It is known as the Mouse Pokémon.'
            ],
            [
                'name' => 'Sealeo',
                'image' => 'pokemon/Sealeo.png',
                'price' => 200000,
                'element_id' => '1',
                'gender' => 'Male',
                'description' => '[Generation 1] Snorlax is a Normal type Pokémon. It is known as the Sleeping Pokémon.'
            ],
            [
                'name' => 'Poochyena',
                'image' => 'pokemon/Poochyena.png',
                'price' => 100000,
                'element_id' => '16',
                'gender' => 'Male',
                'description' => '[Generation 3] Absol is a Dark type Pokémon. It is known as the Disaster Pokémon. 
                                                Absol has a Mega Evolution, available from X & Y onwards.'
            ],
            [
                'name' => 'Vivillon',
                'image' => 'pokemon/Vivillon.png',
                'price' => 200000,
                'element_id' => '5',
                'gender' => 'Male',
                'description' => '[Generation 6] Gogoat is a Grass type Pokémon. It is known as the Mount Pokémon. 
                                                Gogoat is so large that people are able to ride on its back. 
                                                It\'s very calm, and can form a strong bond with its Trainer\'s feelings 
                                                when its Trainer grabs onto its horns.'
            ],
            [
                'name' => 'Steelix',
                'image' => 'pokemon/Steelix.png',
                'price' => 180000,
                'element_id' => '18',
                'gender' => 'Male',
                'description' => '[Generation 3] Kirlia is a Psychic/Fairy type Pokémon. It is known as the Emotion Pokémon. 
                                                    Prior to Generation 6, Kirlia was a pure Psychic type.'
            ],
            [
                'name' => 'Hippowdon',
                'image' => 'pokemon/Hippowdon_(Female).png',
                'price' => 350000,
                'element_id' => '7',
                'gender' => 'Female',
                'description' => '[Generation 4] Lucario is a Fighting/Steel type Pokémon. It is known as the Aura Pokémon. 
                                                    Lucario has a Mega Evolution, available from X & Y onwards.'
            ],
            [
                'name' => 'Igglybuff',
                'image' => 'pokemon/Igglybuff.png',
                'price' => 180000,
                'element_id' => '18',
                'gender' => 'Male',
                'description' => '[Generation 3] Kirlia is a Psychic/Fairy type Pokémon. It is known as the Emotion Pokémon. 
                                                    Prior to Generation 6, Kirlia was a pure Psychic type.'
            ],
            [
                'name' => 'Voltorb',
                'image' => 'pokemon/Voltorb.png',
                'price' => 350000,
                'element_id' => '1',
                'gender' => 'Male',
                'description' => '[Generation 4] Lucario is a Fighting/Steel type Pokémon. It is known as the Aura Pokémon. 
                                                    Lucario has a Mega Evolution, available from X & Y onwards.'
            ],
        ]);
    }
}
