<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'email' => 'admin1@pokemart.com',
                'password' => bcrypt('admin1'),
                'profilePicture' => 'images/admin.png',
                'gender' => 'male',
                'dateOfBirth' => '1990-01-01',
                'address' => 'Wikia fandom',
                'role_id' => '2'
            ],
            [
                'email' => 'admin2@pokemart.com',
                'password' => bcrypt('admin2'),
                'profilePicture' => 'images/admin.png',
                'gender' => 'female',
                'dateOfBirth' => '2000-12-31',
                'address' => 'Pokedex Indonesia',
                'role_id' => '2'
            ],
            [
                'email' => 'user_sample_m@pokemart.com',
                'password' => bcrypt('user123'),
                'profilePicture' => 'images/user_male.png',
                'gender' => 'male',
                'dateOfBirth' => '1980-02-20',
                'address' => 'Rumah User 1',
                'role_id' => '1'
            ],
            [
                'email' => 'user_sample_f@pokemart.com',
                'password' => bcrypt('member123'),
                'profilePicture' => 'images/user_female.png',
                'gender' => 'female',
                'dateOfBirth' => '1979-09-09',
                'address' => 'Rumah Member XYZ',
                'role_id' => '1'
            ],
        ]);
    }
}
