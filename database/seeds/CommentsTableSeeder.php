<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            [
                'pokemon_id' => '1',
                'email' => 'satuduatiga@gmail.com',
                'date' => '2017-10-12 09:34:59',
                'comments' => 'ini comment dengan id 1'
            ],
            [
                'pokemon_id' => '2',
                'email' => 'iniyangkedua@gmail.com',
                'date' => '2017-12-31 11:30:25',
                'comments' => 'ini comment dengan id 2'
            ],
            [
                'pokemon_id' => '1',
                'email' => 'thirdparty@gmail.com',
                'date' => '2017-12-12 11:01:00',
                'comments' => 'ini comment dengan id 3'
            ],
        ]);
    }
}
