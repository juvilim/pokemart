<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(FiltersTableSeeder::class);
        $this->call(PokemonsTableSeeder::class);
        $this->call(ElementsTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(TransactionsTableSeeder::class);
        $this->call(TransactionDetailsTableSeeder::class);
    }
}
