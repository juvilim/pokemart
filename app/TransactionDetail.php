<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'transaction_details';

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }
}
