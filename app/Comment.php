<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'comments';

    public function pokemon()
    {
        return $this->belongsTo(Pokemon::class);
    }
}
