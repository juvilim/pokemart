<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'pokemons';

    public function element()
    {
        return $this->belongsTo(Element::class);
    }
}
