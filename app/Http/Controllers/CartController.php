<?php

namespace App\Http\Controllers;

use DateTime;
use App\Pokemon;
use App\Transaction;
use App\TransactionDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->user =  \Auth::user();
    }

    public function view()
    {
        $cart = session('cart');

        $totalQty = 0;
        $totalPrice = 0;

        if (empty($cart)) {
            return view('cart-list')->with(['items' => [], 'cart' => [], 'totalQty' => 0, 'totalPrice' => 0]);
        } else {
//            $items = Pokemon::find(array_keys($cart));
            $items = Pokemon::whereIn('id', array_keys($cart))->get();

            foreach ($items as $item) {
                $totalQty += $cart[$item['id']];
                $totalPrice += ($cart[$item['id']] * $item->price);
            }

            return view('cart-list')
                ->with(['items' => $items, 'cart' => $cart, 'totalQty' => $totalQty, 'totalPrice' => $totalPrice]);
        }
    }

    public function delete(Request $request, $id)
    {
        $cart = session('cart');

        unset($cart[$id]);

        $request->session()->put('cart',$cart);

        return redirect('/cart/list');
    }

    public function completePayment()
    {
        $cart = session('cart'); // idpokemon => qty

        $transaction = new Transaction();
        $transaction->email = $this->user->email;
        $transaction->date = new DateTime("Asia/Bangkok");
        $transaction->status = 'Pending';
        $transaction->save();

        foreach (array_keys($cart) as $i) {
            $transactionDetail = new TransactionDetail();
            $transactionDetail->transaction_id = $transaction->id;
            $transactionDetail->pokemon_name = Pokemon::find($i)->name;
            $transactionDetail->pokemon_price = Pokemon::find($i)->price;
            $transactionDetail->quantity = $cart[$i];
            $transactionDetail->save();
        }

        session()->flush('cart');

        return redirect('/pokemon/list');
    }
}
