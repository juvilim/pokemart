<?php

namespace App\Http\Controllers;

use App\Element;
use App\Pokemon;
use App\Filter;
use App\Comment;
use Illuminate\Http\Request;
use DateTime;

use Illuminate\Support\Facades\Validator;

class PokemonController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->user =  \Auth::user();
    }

    public function viewAsMember(Request $request)
    {
        // jenis-jenis filter (name , element)
        $categories = Filter::pluck('name', 'id');

        // jenis kategori yang dipilih buat difilter
        $filter = $request->get('searchCategory');

        // isi dari text yang ingin di search
        $keyword = $request->input('keyword');

        // kalau ada keyword yang dimasukkan
        if ($request->has('keyword'))
        {
            // buat simpen value dari dropdown yang terakhir dipilih
            session()->put('category', $filter);

            // name -> $categories[1] , element -> $categories[2]
            if($filter == $categories[1]) {
                // select * from pokemons where name like '%keywordnya%'
                $pokemons = Pokemon::where($categories[1], 'LIKE', '%'.$keyword.'%')->paginate(24);
            } else if($filter == $categories[2]) {
                /*
                    select * from pokemons join elements on pokemons.element_id = elements.id
                    where elements.name = 'keywordnya'
                */
                $pokemons = Pokemon::join('elements', 'pokemons.element_id', '=', 'elements.id')
                            ->select('pokemons.*')
                            ->where('elements.name', '=', $keyword)
                            ->paginate(24);
            }
        } else {
            $pokemons = Pokemon::paginate(24);
        }

        return view('pokemon-list-member')->with('pokemons', $pokemons)->with('keyword', $keyword)->with('categories', $categories);
    }

    public function viewDetail($id)
    {
        /*
            select pokemons.*, elements.name as element_type, email, date, comments from pokemons
            join elements on pokemons.element_id = elememts.id
            where pokemons.id = $id
        */
        $pokemon = Pokemon::join('elements', 'pokemons.element_id', '=', 'elements.id')
            ->select('pokemons.*', 'elements.name AS element_type')
            ->where('pokemons.id', '=', $id)
            ->get();

        /*
            SELECT `comments`.* FROM `comments` WHERE `comments`.`pokemon_id` = $id
        */
        $comments = Comment::where('comments.pokemon_id', '=', $id)->get();

        return view('pokemon-detail')->with('pokemon', $pokemon)->with('comments', $comments);
    }

    public function addToCart(Request $request, $id)
    {
        $rules = [ 'qty' => 'required|numeric|min:1' ];
        $custom = [ 'min' => 'Quantity must be more than 0.' ];
        $validator = Validator::make($request->all(), $rules, $custom);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }


        if($request->session()->has('cart')) {
            $cart = $request->session()->get('cart');
        } else {
            $cart = [];
        }

        if (isset($cart[$id])) $cart[$id] += $request->qty;
        else $cart[$id] = $request->qty;

        $request->session()->put('cart', $cart);
//        $id = $request->qty;
//        $element = Element::find($id);

        return redirect('/home');
    }

    public function addComment(Request $request)
    {
        // validasi
        $rules = [ 'comment' => 'required|min:3' ];
        $custom = [
            'required' => 'Comment must be filled.',
            'min' => 'Comment must be more than 3 characters.'
        ];
        $validator = Validator::make($request->all(), $rules, $custom);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $id = $request->id;

        $comment = new Comment();
        $comment->pokemon_id = $id;
        $comment->email = $this->user->email;
        $comment->date = new DateTime('Asia/Bangkok');
        $comment->comments = $request->comment;
        $comment->save();
//        Comment::insert(['email' => $this->user->email, 'pokemon_id' => $id, 'date' => new DateTime('now'), 'comments' =>  $request->comment]);

        return redirect('/pokemon/detail/' . $id);
    }

    public function viewAsAdmin(Request $request)
    {
        // jenis-jenis filter (name , element)
        $categories = Filter::pluck('name', 'id');

        // jenis kategori yang dipilih buat difilter
        $filter = $request->get('searchCategory');

        // isi dari text yang ingin di search
        $keyword = $request->input('keyword');

        // kalau ada keyword yang dimasukkan
        if ($request->has('keyword'))
        {
            // buat simpen value dari dropdown yang terakhir dipilih
            session()->put('category', $filter);

            // name -> $categories[1] , element -> $categories[2]
            if($filter == $categories[1]) {
                // select * from pokemons where name like '%keywordnya%'
                $pokemons = Pokemon::where($categories[1], 'LIKE', '%'.$keyword.'%')->paginate(24);
            } else if($filter == $categories[2]) {
                /*
                    select * from pokemons join elements on pokemons.element_id = elements.id
                    where elements.name = 'keywordnya'
                */
                $pokemons = Pokemon::join('elements', 'pokemons.element_id', '=', 'elements.id')
                    ->select('pokemons.*')
                    ->where('elements.name', '=', $keyword)
                    ->paginate(24);
            }
        } else {
            $pokemons = Pokemon::paginate(24);
        }

        return view('pokemon-list-admin')->with('pokemons', $pokemons)->with('keyword', $keyword)->with('categories', $categories);
    }

    public function viewInsertForm()
    {
        $elements = Element::all();

        return view('pokemon-insert')->with('elements', $elements);
    }

    public function insert(Request $request)
    {
        $rules = [
            'name' => 'required|alpha|min:3',
            'element' => 'required',
            'image' => 'required|image',
            'gender' => 'required|in:Male,Female',
            'description' => 'required|alpha',
            'price' => 'required|numeric|min:1000'
        ];
        $custom = [
            'required' => ':attribute must be filled.',
            'element.required' => 'Element must be selected.',
            'name.min' => 'Pokemon name must be more than 3 characters.',
            'price.min' => 'Price must be at least 1000.'
        ];
        $validator = Validator::make($request->all(), $rules, $custom);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $pokemon = new Pokemon();
        $pokemon->name = $request->name;

        $filename = $request->image->getClientOriginalName();
        $request->image->move('uploads/pokemon/', $filename);
        $pokemon->image = 'uploads/pokemon/' . $filename;

        $pokemon->price = $request->price;
        $pokemon->element_id = $request->element;
        $pokemon->gender = $request->gender;
        $pokemon->description = $request->description;
        $pokemon->save();

        return redirect('/pokemon/list');
    }

    public function viewUpdateForm(Request $request, $id)
    {
        $pokemon = Pokemon::join('elements', 'pokemons.element_id', '=', 'elements.id')
            ->select('pokemons.*', 'elements.name AS element_type')
            ->where('pokemons.id', '=', $id)
            ->get();

        $elements = Element::all();

        return view('pokemon-update')->with('pokemon', $pokemon)->with('elements', $elements);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|alpha|min:3',
            'element' => 'required',
            'image' => 'required|image',
            'gender' => 'required|in:Male,Female',
            'description' => 'required|alpha',
            'price' => 'required|numeric'
        ];
        $custom = [
            'required' => ':attribute must be filled.'
        ];
        $validator = Validator::make($request->all(), $rules, $custom);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $pokemon = Pokemon::find($id);
        $pokemon->name = $request->name;
        $pokemon->element_id = $request->element;

        $filename = $request->image->getClientOriginalName();
        $request->image->move('uploads/pokemon/', $filename);
        $pokemon->image = 'uploads/pokemon/' . $filename;

        $pokemon->gender = $request->gender;
        $pokemon->description = $request->description;
        $pokemon->price = $request->price;
        $pokemon->save();

        return redirect('/pokemon/list/admin');
    }

    public function delete($id)
    {
        Pokemon::find($id)->delete();
        return redirect('/pokemon/list/admin');
    }
}
