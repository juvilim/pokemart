<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->user =  \Auth::user();
    }

    public function edit()
    {
        $user = $this->user;
        return view('profile-edit')->with('user', $user);
    }

    public function update(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'profilePicture' => 'required|image',
            'gender' => 'required|in:male,female',
            'dateOfBirth' => 'required|date_format:Y-m-d|before:-10 years',
            'address' => 'required|min:10'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $user = $this->user;
        $user->email = $request->email;

        $filename = $request->profilePicture->getClientOriginalName();
        $request->profilePicture->move('uploads/image/', $filename);
        $user->profilePicture = 'uploads/image/'. $filename;

        $user->gender = $request->gender;

//        $user->dateOfBirth = date("Y-m-d", strtotime($request->dateOfBirth));
        $user->dateOfBirth = $request->dateOfBirth;

        $user->address = $request->address;
        $user->save();

        return redirect('/home');
    }
}
