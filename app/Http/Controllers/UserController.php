<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->user =  \Auth::user();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function viewLoginForm()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        if (Auth::viaRemember()) {
            return redirect('/home');
        } else if (Auth::attempt([ 'email' => $request['email'], 'password' => $request['password'] ], $request['remember'])) {
            return redirect('/home');
        }

        return back();
    }

    public function viewRegisterForm()
    {
        return view('register');
    }

//    public function register(Request $request)
//    {
//        // validasi
////        $rules = [
////            'email' => 'required|email|unique:users',
////            'password' => 'alpha_num|min:5',
////            'confirmPassword' => 'required||same:password',
////            'profilePicture' => 'required|image',
////            'gender' => 'in:male,female',
//////            'dateOfBirth' => 'required|date_format:Y-MM-d|before:' . $before,
////            'dateOfBirth' => 'required|date_format:Y-MM-d|before:-10 years',
////            'address' => 'required|min:10'
////
////        ];
////        $custom = [
////            'image' => 'Element name must be filled.',
////            'date_format' => 'Element name must be unique.'
////        ];
////        $validator = Validator::make($request->all(), $rules, $custom);
////dd($validator->fails());
////        if ($validator->fails()) {
////            return redirect()->back()->withErrors($validator);
////        }
////
////        User::create([
////            'email' => $request['email'],
////            'password' => bcrypt($request['password']),
////            'profilePicture' => $request['profilePicture'],
////            'gender' => $request['gender'],
////            'dateOfBirth' => $request['dateOfBirth'],
////            'address' => $request['address']
////        ]);
//        return redirect('/home');
//    }

    public function viewUpdateForm()
    {
        $users = User::where('role_id', '=', '1')->get();

        return view('user-list-update')->with('users', $users);
    }

    public function searchUser(Request $request)
    {
        $rules = [ 'searchEmail' => 'required' ];
        $custom = [ 'required' => 'User email must be filled.' ];
        $validator = Validator::make($request->all(), $rules, $custom);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $id = $request->searchEmail;
        $user = User::find($id);

        return view('user-update')->with('user', $user);
    }

    public function updateUser(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'profilePicture' => 'required|image',
            'gender' => 'required|in:male,female',
            'dateOfBirth' => 'required|date_format:Y-m-d|before:-10 years',
            'address' => 'required|min:10'
        ];
        $custom = [
            'unique' => 'This email already exists.'
        ];
        $validator = Validator::make($request->all(), $rules, $custom);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $id = $request->id;
        $user = User::find($id);
        $user->email = $request->email;

        $filename = $request->profilePicture->getClientOriginalName();
        $request->profilePicture->move('uploads/image/', $filename);
        $user->profilePicture = 'uploads/image/'. $filename;

        $user->gender = $request->gender;
        $user->dateOfBirth = $request->dateOfBirth;
        $user->address = $request->address;
        $user->role_id = '1';
        $user->save();

        return redirect('/home');
    }

    public function viewDeleteForm()
    {
        $users = User::where('role_id', '=', '1')->get();
        return view('user-list-delete')->with('users', $users);
    }

    public function deleteUser(Request $request)
    {
        $rules = [ 'searchEmail' => 'required' ];
        $custom = [ 'required' => 'Eemail must be selected.' ];
        $validator = Validator::make($request->all(), $rules, $custom);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $id = $request->searchEmail;
        User::find($id)->delete();
        return redirect('/home');
    }
}
