<?php

namespace App\Http\Controllers;

use App\Element;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

class ElementController extends Controller
{
    public function view()
    {
        $elements = Element::all();

        return view('element-list')->with('elements', $elements);
    }

    public function create()
    {
        return view('element-insert');
    }

    public function add(Request $request)
    {
        // validasi
        $rules = [ 'name' => 'required|unique:elements' ];
        $custom = [
            'required' => 'Element name must be filled.',
            'unique' => 'Element name must be unique.'
        ];
        $validator = Validator::make($request->all(), $rules, $custom);

        if ($validator->fails()) {
            return redirect('element/create')->withErrors($validator);
        }

        // INSERT INTO Element (name) VALUES ($element->name)
        /* Element::insert(['name' => $element->name]); */
        $element = new Element();
        $element->name = $request->name;
        $element->save();

        return redirect('/element');
    }

    public function edit(Request $request)
    {
        $rules = [ 'searchElement' => 'required' ];
        $custom = [ 'required' => 'Element must be selected.' ];
        $validator = Validator::make($request->all(), $rules, $custom);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $id = $request->searchElement;
        $element = Element::find($id);

        return view('element-update')->with('element', $element);
    }

    public function update(Request $request)
    {
        $rules = [ 'name' => 'required|unique:elements' ];
        $custom = [
            'required' => 'Element name must be filled.',
            'unique' => 'Element name must be unique.'
        ];
        $validator = Validator::make($request->all(), $rules, $custom);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        // UPDATE Element SET name = $element->name WHERE id = $id
        /* Element::where('id', $id)->update(['name' => $request->name]); */
        $id = $request->id;
        $element = Element::find($id);
        $element->name = $request->name;
        $element->save();

        return redirect('/element');
    }
}
