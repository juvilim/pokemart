<?php

namespace App\Http\Controllers\Auth;
use App\Http\Requests\Request;
use Faker\Provider\DateTime;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $redirectAfterLogout = '/login';

//    protected $guard = 'admin';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:5|alpha_num|regex:/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/',
            'confirmPassword' => 'required|same:password',
            'profilePicture' => 'required|image',
            'gender' => 'required|in:male,female',
            'dateOfBirth' => 'required|date_format:Y-m-d|before:-10 years',
            'address' => 'required|min:10'
            ]);

//        return Validator::make($data, [
//            'name' => 'required|max:255',
//            'email' => 'required|email|max:255|unique:users',
//            'password' => 'required|min:6|confirmed',
//        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $filename = $data['profilePicture']->getClientOriginalName();
        $data['profilePicture']->move('uploads/image/', $filename);

        User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'profilePicture' => 'uploads/image/' . $filename,
            'gender' => $data['gender'],
            'dateOfBirth' => $data['dateOfBirth'],
            'address' => $data['address'],
            'role_id' => '1'
        ]);

    }

    public function logout()
    {
        Auth::guard($this->getGuard())->logout();
        session()->forget('cart');
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }
}
