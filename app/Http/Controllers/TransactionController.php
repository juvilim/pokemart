<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\TransactionDetail;
use Illuminate\Http\Request;

use App\Http\Requests;

class TransactionController extends Controller
{
    public function view()
    {
        $transactions = Transaction::all();

        return view('transaction-list')->with('transactions', $transactions);
    }

    public function viewDetail()
    {
        $transactionDetails = TransactionDetail::all();

        $totalQty = 0;
        $totalPrice = 0;

        foreach ($transactionDetails as $transactionDetail) {
            $totalQty += $transactionDetail->quantity;
            $totalPrice += ($transactionDetail->pokemon_price * $transactionDetail->quantity);
        }

        return view('transaction-detail')->with(['transactionDetails' => $transactionDetails, 'totalQty' => $totalQty, 'totalPrice' => $totalPrice]);
    }

    public function update(Request $request, $id)
    {
        $transaction = Transaction::find($id);
        $transaction->status = $request->status;
        $transaction->save();

        return redirect('/home');
    }

    public function viewDeleteForm()
    {
        $transactions = Transaction::all();

        return view('transaction-delete')->with('transactions', $transactions);
    }

    public function delete($id)
    {
        Transaction::find($id)->delete();
        return redirect('/home');
    }


}
