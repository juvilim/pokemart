<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Home (DONE) */
Route::get('/', function () {
    return redirect('/home');
});
Route::get('/home', function () {
    return view('welcome');
});

//Route::get('/home', 'HomeController@index');
//Route::get('/home', 'UserController@index');

/* Login - Guest (remember me belom, belom cek lagi) */
Route::get('/login', 'UserController@viewLoginForm');
Route::post('/login', 'UserController@login');

/* Register - Guest (DONE) */
//Route::get('/register', 'UserController@viewRegisterForm');
//Route::post('/register/done', 'UserController@register');

/* Profile - Member (DONE) */
//Route::get('/profile/edit', [
//    'middleware' => 'auth',
//    'uses' => 'ProfileController@edit'
//]);
//Route::put('/profile', [
//    'middleware' => 'auth',
//    'uses' => 'ProfileController@update'
//]);
Route::get('/profile/edit', 'ProfileController@edit');
Route::put('/profile', 'ProfileController@update');

/* Pokemon - Member (DONE) */
Route::get('/pokemon/list', 'PokemonController@viewAsMember');
Route::get('/pokemon/detail/{id}', 'PokemonController@viewDetail');
Route::post('/pokemon/detail/comment/{id}', 'PokemonController@addComment');
Route::get('/cart/add/{id}', 'PokemonController@addToCart');

/* Pokemon - Admin (DONE) */
Route::get('/pokemon/list/admin', 'PokemonController@viewAsAdmin');
Route::get('/pokemon/insert', 'PokemonController@viewInsertForm');
Route::post('/pokemon/insert', 'PokemonController@insert');
Route::get('/pokemon/update/{id}', 'PokemonController@viewUpdateForm');
Route::put('/pokemon/update/{id}', 'PokemonController@update');
Route::delete('/pokemon/delete/{id}', 'PokemonController@delete');

/* Element (DONE) */
Route::get('/element', 'ElementController@view');
Route::get('/element/create', 'ElementController@create');
Route::post('/element', 'ElementController@add');
Route::get('/element/edit', 'ElementController@edit');
Route::put('/element/{id}', 'ElementController@update');

/* Cart - Member (DONE)*/
Route::get('/cart/list', 'CartController@view');
Route::get('/cart/create', 'CartController@create');
Route::get('/cart/delete/{id}', 'CartController@delete');
Route::get('/cart/payment/pending', 'CartController@completePayment');


/* User - Admin (DONE) */
Route::get('/user/update', 'UserController@viewUpdateForm');
Route::get('/user/update/search', 'UserController@searchUser');
Route::put('/user/update/{id}', 'UserController@updateUser');
Route::get('/user/delete', 'UserController@viewDeleteForm');
Route::delete('/user/delete', 'UserController@deleteUser');

/* Transaction (DONE) */
Route::get('/transaction/list/update', 'TransactionController@view');
Route::get('/transaction/detail/{id}', 'TransactionController@viewDetail');
Route::put('/transaction/list/update/{id}', 'TransactionController@update');
Route::get('/transaction/list/delete', 'TransactionController@viewDeleteForm');
Route::delete('/transaction/list/delete/{id}', 'TransactionController@delete');

Route::get('/logout', 'Auth\AuthController@logout');

/* Auth */
Route::auth();
//
