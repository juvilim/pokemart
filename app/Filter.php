<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'filters';
}
