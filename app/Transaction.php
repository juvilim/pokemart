<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function transactionDetail()
    {
        return $this->belongsTo(TransactionDetail::class);
    }
}
